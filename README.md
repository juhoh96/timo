Tämä on Olio-ohjelmoinnin harjoitustyönä toteuttamani TIMO-järjestelmän repository.

Dokumentaatio: https://drive.google.com/file/d/1OiADG5dum-WxeFr7QBjHPj3C5ZA7U7rC/view?usp=sharing

Toimintaa esittelevä video: http://screencast-o-matic.com/watch/cbXtoq2vJd



UPDATE 7.12 - Lisätty pyydetyt ominaisuudet:

-Varastotietoihin tieto siitä, onko paketti mennyt matkalla rikki (lähetetyt), sekä tieto siitä, voiko paketti mennä rikki (lähettämättömät).

-Vain yksi paketinluonti- tai varastotieto-ikkuna on mahdollista avata kerrallaan.