/*
*
* 2017 © Juho Haapalainen
*
 */
package timo;


import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;
import javafx.stage.Stage;


public class FXMLTIMOController implements Initializable {

    @FXML
    private WebView web;
    @FXML
    private Button createParcelButton;
    @FXML
    private Button sendParcelButton;
    @FXML
    private ComboBox<String> cityCombo;
    @FXML
    private Button addSmartPostsToMapButton;
    @FXML
    public ComboBox<Parcel> storageCombo;
    @FXML
    private Button deletePathsButton;
    @FXML
    private TextField timoScreen;
    @FXML
    private Button startStorageViewButton;
    @FXML
    private Button saveStorageDataButton;
    @FXML
    private Button loadStorageDataButton;
    @FXML
    private Label timoLabel;
    
    // An instance of SmartPostData includes all the data loaded from the XML-file
    SmartPostData spd = SmartPostData.getInstance();
    Storage storage = Storage.getInstance();
    SerOperator so = new SerOperator();
    StorageData storageData = StorageData.getInstance();

    ArrayList<SmartPost> smartPosts = spd.getSmartPosts();
    ArrayList<String> cities = spd.getCities();
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        web.getEngine().load(getClass().getResource("index.html").toExternalForm());
        fillCityCombo();
        timoScreen.setText("Tervetuloa käyttämään TIMO-järjestelmää!");
    }    
    
    // Function fills the combobox that asks user to choose the city
    private void fillCityCombo() {
        for (String city : cities) {
            cityCombo.getItems().add(city);
        }
    }    
    
    // Adds those SmartPosts to map that locate in the city which user has chosen from combobox
    @FXML
    private void addSmartPostsToMap(ActionEvent event) {
        for (SmartPost sp : smartPosts) {
            if (cityCombo.getValue().equals(sp.getCity())) {
                addSmartPostToMap(sp);
            }
        }
        timoScreen.setText("Kaupungin " + cityCombo.getValue() + " SmartPost-automaatit lisätty kartalle.");
    }
    
    // Adds an individual SmartPost to map
    private void addSmartPostToMap(SmartPost sp) {
        String cityFirstLetterUpperCase = sp.getCity().toLowerCase().substring(0, 1).toUpperCase() + sp.getCity().toLowerCase().substring(1);
        String addToMapScript = "document.goToLocation('" + sp.getAddress() + ", " + sp.getCode() + " " + cityFirstLetterUpperCase  + "', '" + sp.getPostoffice() + ", " + "Auki: " + sp.getAvailability() + "', 'blue')";
        web.getEngine().executeScript(addToMapScript);
    }

    // Function fills the combobox from which user can choose parcels to send
    @FXML
    private void fillStorageCombo(Event event) {
        storageCombo.getItems().clear();
        for (Parcel p : storage.parcels) {
            storageCombo.getItems().add(p);
        }
    }

    // Function sends parcel by creating a path for it and removing it from storage
    @FXML
    private void sendParcel(ActionEvent event) {        
        
        updateDistances();
        Parcel departingParcel = storageCombo.getValue();
        
        // If the delivery distance is longer than maximum delivery distance (150 km for the first class),
        // the parcel won't be sent. If the maximum delivery distance is 0, then the parcel can be sent as
        // far as user wants.
        if (departingParcel.getMaxDeliveryDistance() == 0 | departingParcel.getMaxDeliveryDistance() > departingParcel.getDistance()) {
            
            // Depending on the parcel class and item inside it, the function exposeToTIMOManAggression() 
            // either breaks the item or lets it remain unbroken. The result will appear in the timoScreen.
            departingParcel.exposeToTIMOManAggression();
            if (departingParcel.getItem().isBroken == false) {
                timoScreen.setText("Paketti lähetettiin ja sisältö säilyi ehjänä.");
            } else {
                timoScreen.setText("Paketti lähetettiin, mutta sisältö meni rikki matkalla. ");
            }
            
            // Creating the path for the parcel.
            String createPathScript = createPathScript(departingParcel);
            web.getEngine().executeScript(createPathScript);
            addSmartPostToMap(storageCombo.getValue().getDepartureSP());
            addSmartPostToMap(storageCombo.getValue().getArrivalSP());

            // Removing parcel from storage, and counting the sent parcel in storageData.
            storageData.sentParcels.add(departingParcel);
            storageData.parcelsInStorage.remove(departingParcel);
            storage.parcels.remove(departingParcel);
            storageCombo.getItems().remove(departingParcel);

        } else {
            // If the distance was longer than the maximum delivery distance, the parcel will be deleted.
            timoScreen.setText("Liian pitkä matka. Paketti poistetaan.");
            storageData.parcelsInStorage.remove(departingParcel);
            storage.parcels.remove(departingParcel);
            storageCombo.getItems().remove(departingParcel);
        }
    }
    
    // Creating the javascript command for creating path for the given parcel
    private String createPathScript(Parcel p) {       
        ArrayList geoInfo = new ArrayList();
        geoInfo.add(Double.toString(p.getDepartureSP().getGeoPoint().getLat()));
        geoInfo.add(Double.toString(p.getDepartureSP().getGeoPoint().getLng()));
        geoInfo.add(Double.toString(p.getArrivalSP().getGeoPoint().getLat()));
        geoInfo.add(Double.toString(p.getArrivalSP().getGeoPoint().getLng()));

        int parcelClass = p.getParcelClass();  
        return "document.createPath(" + geoInfo + ", '" + "blue" + "', " + parcelClass + ")";        
    }
    
    // Creating the javascript command for calculating the delivery distance
    private String createDistanceScript(Parcel p) {       
        ArrayList geoInfo = new ArrayList();
        geoInfo.add(Double.toString(p.getDepartureSP().getGeoPoint().getLat()));
        geoInfo.add(Double.toString(p.getDepartureSP().getGeoPoint().getLng()));
        geoInfo.add(Double.toString(p.getArrivalSP().getGeoPoint().getLat()));
        geoInfo.add(Double.toString(p.getArrivalSP().getGeoPoint().getLng()));  
        return "document.getDistance(" + geoInfo + ")";        
    }

    // Function clears all the paths from the map.
    @FXML
    private void deletePaths(ActionEvent event) {
        web.getEngine().executeScript("document.deletePaths()");
        timoScreen.setText("Tyhjennettiin reitit kartalta.");
    }
    
    // Opens the CreateParcelView window.
    @FXML
    private void startCreateParcelView(ActionEvent event) {
        try {
            Stage createParcelView = new Stage();
            Parent parcelPage = FXMLLoader.load(getClass().getResource("FXMLCreateParcelView.fxml"));            
            Scene scene = new Scene(parcelPage);
            String css = Mainclass.class.getResource("timoStyle.css").toExternalForm();
            scene.getStylesheets().add(css);
            createParcelView.setScene(scene);
            createParcelView.show();                        
        } catch (IOException ex) {
            Logger.getLogger(FXMLTIMOController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Opens the StorageView window.
    @FXML
    private void startStorageView(ActionEvent event) {
        updateDistances();
        try {
            Stage storageView = new Stage();
            Parent statisticsPage = FXMLLoader.load(getClass().getResource("FXMLStorageView.fxml"));            
            Scene scene = new Scene(statisticsPage);
            storageView.setScene(scene);
            storageView.show();                        
        } catch (IOException ex) {
            Logger.getLogger(FXMLStorageViewController.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    // Serializes the storage data and saves it in file "storagedata.ser"
    @FXML
    private void saveStorageDataAction(ActionEvent event) {
        so.saveStorageData(storageData);
        timoScreen.setText("Varastotiedot tallennettu.");
    }
    
    // Loads storage data from file "storagedata.ser"
    @FXML
    private void loadStorageDataAction(ActionEvent event) {
        storageData = so.loadStorageData();
        for (Parcel p : storageData.parcelsInStorage) {
            storage.parcels.add(p);
        }
        storage.counter = storageData.parcelsInStorage.size() + storageData.sentParcels.size() + 1;
        timoScreen.setText("Ladattiin varastotiedot.");        
    }
    
    // Function calculates the delivery distances for all parcels as it is easier to do it
    // here than in CreateParcelView.
    private void updateDistances() { 
        int distance;
        for (Parcel p : storage.parcels) {
            distance = (int)(double) web.getEngine().executeScript(createDistanceScript(p));
            p.setDistance(distance);
        }
        for (Parcel p : storageData.parcelsInStorage) {
            distance = (int)(double) web.getEngine().executeScript(createDistanceScript(p));
            p.setDistance(distance);
        }
        for (Parcel p : storageCombo.getItems()) {
            distance = (int)(double) web.getEngine().executeScript(createDistanceScript(p));
            p.setDistance(distance);
        }        
    }

}