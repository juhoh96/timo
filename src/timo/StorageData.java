/*
*
* 2017 © Juho Haapalainen
*
 */
package timo;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

// A class that contains information of already sent parcels and the parcels in storage. 
// The class is used for showing the information in TableView-components in StorageView.
public class StorageData implements java.io.Serializable{
    
    public ObservableList<Parcel> sentParcels;
    public ObservableList<Parcel> parcelsInStorage;
    
    static private StorageData storageData = null;
    
    private StorageData() {
        sentParcels = FXCollections.observableArrayList();
        parcelsInStorage = FXCollections.observableArrayList();
    }
    
    static public StorageData getInstance() {
        if (storageData == null) {
            storageData = new StorageData();
        }
        return storageData;
    }

}
