/*
*
* 2017 © Juho Haapalainen
*
 */
package timo;


public abstract class Parcel implements java.io.Serializable { 

    protected Item item; // The parcel can contain one item
    protected String ID; // ID in format "xxxx" (e.g. 0003)
    protected double maxSize; // The maximum size of the item inside it
    protected double maxWeight; // The maximum weight of the item inside it
    protected int parcelClass; // First, second or third class
    protected String departureCity;
    protected String arrivalCity;
    protected SmartPost departureSP; // SP = SmartPost
    protected SmartPost arrivalSP;
    protected int maxDeliveryDistance; // The longest distance the parcel can be send
    private int distance; // The distance between departure SmartPost and arrival SmartPost
    
    // Function breaks item inside the parcel if it's breakable
    public void tryToBreakItem() {
        if (this.item.isBreakable) {
            this.item.isBroken = true;
        }           
    }
    
    // Depending on the parcel class, the function (or the TIMO-man) tries to break the item inside it.
    public void exposeToTIMOManAggression() {
        switch (this.parcelClass) {
            // Item inside the parcel breaks if it's breakable
            case 1:
                this.tryToBreakItem();
                break;
            // Nothing happens because 2. class is the safest option
            case 2:
                break;
            // Item inside the parcel breaks if it's breakable and it's not big and heavy enough.
            case 3:
                if (this.item.getSize() <= 3.0 && this.item.getWeight() <= 5.0) {
                    this.tryToBreakItem();
                }   break;
            default:
                break;
        }
    }
    
    public Item getItem() {
        return item;
    }
    
    public String getID() {
        return ID;
    }
    
    public double getMaxSize() {
        return maxSize;
    }

    public double getMaxWeight() {
        return maxWeight;
    }
    
    public int getParcelClass() {
        return parcelClass;
    }
    
    public SmartPost getDepartureSP() {
        return departureSP;
    }
    
    public SmartPost getArrivalSP() {
        return arrivalSP;
    }
    
    public String getDepartureCity() {
        return departureCity;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }
   
    public int getMaxDeliveryDistance() {
        return maxDeliveryDistance;
    }
    
    public int getDistance() {
        return distance;
    }
    
    public void setItem(Item i) {
        this.item = i;
    }
    
    public void setDepartureSP(SmartPost departureSP) {
        this.departureSP = departureSP;
    }
    
    public void setArrivalSP(SmartPost arrivalSP) {
        this.arrivalSP = arrivalSP;
    }
    
    public void setDepartureCity(String depCity) {
        this.departureCity = depCity;
    }
    
    public void setArrivalCity(String arrCity) {
        this.arrivalCity = arrCity;
    }

    public void setMaxDeliveryDistance(int maxDeliveryDistance) {
        this.maxDeliveryDistance = maxDeliveryDistance;
    }
    
    public void setDistance(int distance) {
        this.distance = distance;
    }
    
    @Override
    public String toString() {
        return parcelClass + ".lk paketti, " + departureSP.getCity() + " - " + arrivalSP.getCity();
    }
}

// Only first class parcel has maxDeliveryDistance, the others can be sent anywhere. Fastest option.
class FirstClassParcel extends Parcel implements java.io.Serializable { 

    public FirstClassParcel(String id) {        
        this.ID = id;
        this.parcelClass = 1;
        this.maxSize = 5.0;
        this.maxWeight = 10.0;
        this.maxDeliveryDistance = 150;
    }    
}

// Second class can contain very large objects (e.g. Karjala 100-pack). Safest option.
class SecondClassParcel extends Parcel implements java.io.Serializable {
    
    public SecondClassParcel(String id) {        
        this.ID = id;
        this.parcelClass = 2;
        this.maxSize = 100.0;
        this.maxWeight = 50.0;        
    }    
}

// The slowest option
class ThirdClassParcel extends Parcel implements java.io.Serializable {
    
    public ThirdClassParcel(String id) {        
        this.ID = id;
        this.parcelClass = 3;
        this.maxSize = 5.0;
        this.maxWeight = 10.0;        
    }   
}