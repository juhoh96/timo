/*
*
* 2017 © Juho Haapalainen
*
 */
package timo;

// A SmartPost-class that contains information of an individual SmartPost.
public class SmartPost implements java.io.Serializable {
   
    private String code;
    private String city;
    private String address;    
    private String availability;
    private String postoffice;
    private GeoPoint geoPoint; 
    
    // A subclass that contains information of the geographic location.
    public class GeoPoint implements java.io.Serializable {

        private double lat;
        private double lng;
        
        public GeoPoint(double lat, double lng) {
            this.lat = lat; // Latitude
            this.lng = lng; // Longitude
        }        
                
        public double getLat() {
            return lat;
        }
      
        public double getLng() {
            return lng;
        }        
    }
    
    public SmartPost(String cd, String ct, String ad, String av, String po, double lat, double lng) {
        this.code = cd;
        this.city = ct;
        this.address = ad;
        this.availability = av;
        this.postoffice = po;
        this.geoPoint = new GeoPoint(lat, lng);
    }

    public String getCode() {
        return code;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getAvailability() {
        return availability;
    }

    public String getPostoffice() {
        return postoffice;
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }
    
    @Override
    public String toString() {
        return this.postoffice;
    }
       
}
